<?xml version='1.0'?>
<Project Type="Project" LVVersion="8008005">
   <Item Name="My Computer" Type="My Computer">
      <Property Name="CCSymbols" Type="Str">OS,Win;CPU,x86;</Property>
      <Property Name="server.app.propertiesEnabled" Type="Bool">true</Property>
      <Property Name="server.control.propertiesEnabled" Type="Bool">true</Property>
      <Property Name="server.tcp.enabled" Type="Bool">false</Property>
      <Property Name="server.tcp.port" Type="Int">0</Property>
      <Property Name="server.tcp.serviceName" Type="Str">My Computer/VI Server</Property>
      <Property Name="server.tcp.serviceName.default" Type="Str">My Computer/VI Server</Property>
      <Property Name="server.vi.callsEnabled" Type="Bool">true</Property>
      <Property Name="server.vi.propertiesEnabled" Type="Bool">true</Property>
      <Property Name="specify.custom.address" Type="Bool">false</Property>
      <Item Name="elements" Type="Folder">
         <Item Name="pouring-element.vi" Type="VI" URL="pouring-element.vi"/>
         <Item Name="random-element.vi" Type="VI" URL="random-element.vi"/>
         <Item Name="percentage-to-value-element.vi" Type="VI" URL="percentage-to-value-element.vi"/>
         <Item Name="writeReport-element.vi" Type="VI" URL="writeReport-element.vi"/>
         <Item Name="dec-inc-element.vi" Type="VI" URL="dec-inc-element.vi"/>
         <Item Name="value-to-percentage-element.vi" Type="VI" URL="value-to-percentage-element.vi"/>
      </Item>
      <Item Name="pvc-process.vi" Type="VI" URL="pvc-process.vi"/>
      <Item Name="Dependencies" Type="Dependencies"/>
      <Item Name="Build Specifications" Type="Build"/>
   </Item>
</Project>
